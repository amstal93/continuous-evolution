package downloader

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"testing"
)

func TestNewManager(t *testing.T) {
	m := NewManager(DefaultConfig, mocks.NewFakeRetrieveProjectDefaultFatal(t), mocks.NewFakeDeleteProjectDefaultFatal(t))
	if len(m.(*manager).downloaders) != 4 {
		t.Fatal("By default all downloader should be activated")
	}
	if _, ok := m.(*manager).downloaders[2].(git); !ok {
		t.Fatal("Git downloader should be after pull and merge request downloaders")
	}
}

func TestNewManagerWithConfig(t *testing.T) {
	config := Config{Git: GitConfig{Enabled: false}, Mergerequest: MergeRequestConfig{Enabled: true}, Pullrequest: PullRequestConfig{Enabled: true}, Local: LocalConfig{Enabled: true}}
	m := NewManager(config, mocks.NewFakeRetrieveProjectDefaultFatal(t), mocks.NewFakeDeleteProjectDefaultFatal(t))
	if len(m.(*manager).downloaders) != 3 {
		t.Fatal("If git is disabled only 3 downloaders should be build")
	}
}

type fakeDownloader struct {
	acceptURL string
}

func (f fakeDownloader) accept(url string) bool { return f.acceptURL == url }
func (fakeDownloader) buildProject(url string) (project.Project, error) {
	return project.New(project.Input{GitURL: url}), nil
}
func (fakeDownloader) download(p project.Project) (project.Project, error) { return p, nil }

func TestManagerBuildProject(t *testing.T) {
	m := NewManager(DefaultConfig, mocks.NewFakeRetrieveProjectDefaultFatal(t), mocks.NewFakeDeleteProjectDefaultFatal(t))
	m.(*manager).downloaders = []downloader{fakeDownloader{acceptURL: "test"}, newGit(DefaultConfig)}
	p, err := m.BuildProject("test")
	if err != nil {
		t.Fatal("Valid conf should not throw err", err)
	}
	if p.GitURL() != "test" {
		t.Fatal("Manager should return project from downloader")
	}
	_, err = m.BuildProject("")
	if err == nil {
		t.Fatal("No downloader available for url should throw error from manager")
	}
}

func TestManagerDownloaderProject(t *testing.T) {
	m := NewManager(DefaultConfig, mocks.NewFakeRetrieveProjectDefaultFatal(t), mocks.NewFakeDeleteProjectDefaultFatal(t))
	m.(*manager).downloaders = []downloader{fakeDownloader{acceptURL: "test"}, newGit(DefaultConfig)}
	p := m.Download(project.NewOptional(project.New(project.Input{GitURL: "test"})))
	if p.IsError() {
		t.Fatal("Valid conf should not throw err", p.Err())
	}
	if p.Get().GitURL() != "test" {
		t.Fatal("Manager should return project from downloader")
	}
	p = m.Download(project.NewOptional(project.New(project.Input{GitURL: ""})))
	if !p.IsError() {
		t.Fatal("No downloader available for url should throw error from manager")
	}
}
