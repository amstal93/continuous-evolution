package project

//go:generate charlatan -package mocks -output ../mocks/project.go Project HTTP Git

import (
	"continuous-evolution/src/project/io"
	"fmt"
	"strings"
)

//TypeHost represent a git server which can be used with conevol
type TypeHost string

const (
	//Gitlab is the TypeHost for a gitlab instance
	Gitlab TypeHost = "gitlab"
	//Github is the TypeHost for a github instance
	Github TypeHost = "github"
	//Unknown is the TypeHost for an unknown git server, will do nothing
	Unknown TypeHost = "unknown"
	//Local is a local git repository
	Local TypeHost = "local"
)

//Project represent the main model of applications.
type Project interface {
	Fullname() string //Fullname is host/organisation/name of project
	AbsoluteDirectoy() string
	HasExcludes(pkg Package) []string

	GitURL() string // All sub-properties are derivated from this. Find a way to do better
	TypeHost() TypeHost
	Host() string
	Login() string
	Token() string
	Organisation() string
	Name() string
	ReProcessDistantID() string

	URLReProcess() string // TODO move this part == only used in reporter for merge-request template
	BranchName() string
	Excludes() map[string][]string
	Packages() []Package

	Git() Git
	HTTP() HTTP

	IsExcludesPkg(pkg Package) bool
	IsExcludesPkgDep(pkg Package, dep Dependency) bool
	IsSame(host string, organisation string, name string) bool

	Copy() Project
	SetReProcessDistantID(reProcessDistantID string) Project
	SetURLReProcess(urlReProcess string) Project // TODO move this part == only used in reporter for merge-request template
	SetGitURL(GitURL string) Project
	SetExcludes(excludes map[string][]string) Project
	SetPackages(packages []Package) Project
}

//Git is a git helper linked with current project
type Git interface {
	Clone() error
	Diff() bool
	Commit() error
	PushForce() error
	DefaultBranch() (string, error)
	TrackFile(path string) (bool, error)
}

//HTTP is an http client helper linked with project (login/token basic auth)
type HTTP interface {
	Get(url string, unmarshalJson interface{}) error
	Post(url string, marshalJson interface{}, unmarshalJson interface{}) error
	Patch(url string, marshalJson interface{}, unmarshalJson interface{}) error
	Put(url string, marshalJson interface{}, unmarshalJson interface{}) error
}

type project struct {
	http *io.HTTP
	git  *io.Git

	name               string
	gitURL             string
	branchName         string
	pathToWrite        string
	packages           []Package
	login              string
	token              string
	typeHost           TypeHost
	host               string
	organisation       string
	excludes           map[string][]string
	reProcessDistantID string //id of merge-request or pull-request
	urlReProcess       string
}

//Input is the struct to give to project.New to build a new Project
type Input struct {
	Name         string
	GitURL       string
	BranchName   string
	PathToWrite  string
	Login        string
	Token        string
	TypeHost     TypeHost
	Host         string
	Organisation string
}

//New build a project
func New(input Input) Project {
	return &project{
		http: io.NewHTTP(input.Login, input.Token),
		git:  io.NewGit(input.PathToWrite, input.Name, input.GitURL, input.BranchName),

		name:               input.Name,
		gitURL:             input.GitURL,
		branchName:         input.BranchName,
		pathToWrite:        input.PathToWrite,
		packages:           make([]Package, 0),
		login:              input.Login,
		token:              input.Token,
		host:               input.Host,
		typeHost:           input.TypeHost,
		organisation:       input.Organisation,
		excludes:           make(map[string][]string),
		reProcessDistantID: "",
		urlReProcess:       "",
	}
}

func (p *project) AbsoluteDirectoy() string {
	return fmt.Sprintf("%s/%s", p.pathToWrite, p.name)
}

func (p *project) Fullname() string {
	return fmt.Sprintf("%s/%s/%s", p.host, p.organisation, p.name)
}

func (p *project) IsExcludesPkg(pkg Package) bool {
	values, ok := p.excludes[pkg.CleanPath(p)]
	return ok && len(values) == 0
}

func (p *project) HasExcludes(pkg Package) []string {
	values, ok := p.excludes[pkg.CleanPath(p)]
	if ok {
		return values
	}
	return []string{}
}

func (p *project) IsExcludesPkgDep(pkg Package, dep Dependency) bool {
	values, ok := p.excludes[pkg.CleanPath(p)]
	if ok && len(values) > 0 {
		for _, d := range values {
			if dep.Name == d {
				return true
			}
		}
	}
	return false
}

func (p *project) HTTP() HTTP {
	return p.http
}

func (p *project) GitURL() string {
	return p.gitURL
}

func (p *project) Git() Git {
	return p.git
}

func (p *project) TypeHost() TypeHost {
	return p.typeHost
}

func (p *project) Host() string {
	return p.host
}

func (p *project) Login() string {
	return p.login
}

func (p *project) Token() string {
	return p.token
}

func (p *project) URLReProcess() string {
	return p.urlReProcess
}

func (p *project) ReProcessDistantID() string {
	return p.reProcessDistantID
}

func (p *project) Organisation() string {
	return p.organisation
}

func (p *project) Name() string {
	return p.name
}

func (p *project) BranchName() string {
	return p.branchName
}

func (p *project) Excludes() map[string][]string {
	return p.excludes
}

func (p *project) Packages() []Package {
	return p.packages
}

//Copy make a clone of current project
func (p *project) Copy() Project {
	return p.copy()
}

func (p *project) copy() *project {
	return &project{
		http:               p.http,
		git:                p.git,
		name:               p.name,
		gitURL:             p.gitURL,
		branchName:         p.branchName,
		pathToWrite:        p.pathToWrite,
		packages:           p.packages,
		login:              p.login,
		token:              p.token,
		host:               p.host,
		typeHost:           p.typeHost,
		organisation:       p.organisation,
		excludes:           p.excludes,
		reProcessDistantID: p.reProcessDistantID,
		urlReProcess:       p.urlReProcess,
	}
}

//SetGitURL set git url to clone project.
//Following immutability principe this function return a new project.
func (p *project) SetGitURL(GitURL string) Project {
	copy := p.copy()
	copy.git = io.NewGit(p.pathToWrite, p.name, GitURL, p.branchName)
	copy.gitURL = GitURL
	return copy
}

//SetURLReProcess set a url to relaunch process.
//Following immutability principe this function return a new project.
func (p *project) SetURLReProcess(urlReProcess string) Project {
	copy := p.copy()
	copy.urlReProcess = urlReProcess
	return copy
}

//SetReProcessDistantID set an id of merge-request or pull-request to relaunch process.
//Following immutability principe this function return a new project.
func (p *project) SetReProcessDistantID(reProcessDistantID string) Project {
	copy := p.copy()
	copy.reProcessDistantID = reProcessDistantID
	return copy
}

//SetPackages permit to set packages to a project.
//Following immutability principe this function return a new project.
func (p *project) SetPackages(packages []Package) Project {
	copy := p.copy()
	copy.packages = packages
	return copy
}

//SetExcludes set excludes to a copy project and return it
func (p *project) SetExcludes(excludes map[string][]string) Project {
	copy := p.copy()
	copy.excludes = excludes
	return copy
}

//IsSame compare project by typeHost, organisation and name
func (p *project) IsSame(host string, organisation string, name string) bool {
	return p.host == host && p.organisation == organisation && p.name == name
}

//PackageType is a string representing a kind of package like npm, maven, pip, docker...
type PackageType string

//Package is a PackageType and a path to find main file line package.json, pom.xml... The path can be a directory or a file depending on the package.
type Package struct {
	Type                PackageType  `json:"type"`
	Path                string       `json:"relativePath"`
	UpdatedDependencies []Dependency `json:"updatedDependencies"`
}

//AddUpdatedDependencies add updated dependencies in package.
//Following immutability principe this function return a new project.
func (p Package) AddUpdatedDependencies(dependencies []Dependency) Package {
	return Package{
		Type:                p.Type,
		Path:                p.Path,
		UpdatedDependencies: append(p.UpdatedDependencies, dependencies...),
	}
}

//CleanPath return path to the pkg without /tmp/ dir.
//Used in template of MergeRequest
func (p Package) CleanPath(project Project) string {
	return strings.TrimPrefix(p.Path, project.AbsoluteDirectoy()+"/")
}

//IsExcludes return true when the pkg is excluded (and no dependencies are excluded)
//Used in template of MergeRequest
func (p Package) IsExcludes(project Project) bool {
	return project.IsExcludesPkg(p)
}

//Dependency is a dependency including in a Package
type Dependency struct {
	Name            string `json:"name"`
	NewVersion      string `json:"newVersion"`
	OriginalVersion string `json:"originalVersion"`
	CanBeExcludes   bool   `json:"canBeExcludes"`
}

//IsExcludes return true when the dependency is excluded (and the pkg is not)
//Used in template of MergeRequest
func (d Dependency) IsExcludes(pkg Package, project Project) bool {
	return project.IsExcludesPkgDep(pkg, d)
}
