package io

import "os"

//PathExists return true is the path is a directory or a file in the filesystem
func PathExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}
