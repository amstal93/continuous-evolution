package orchestrator

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"errors"
	"testing"
)

func TestStartCli(t *testing.T) {
	fakeProject := mocks.NewFakeProjectDefaultFatal(t)

	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeSender.SendHook = func(o project.Option) {
		if o.Get() != fakeProject {
			t.Fatal("Bad project receive")
		}
	}

	fakeDownloadManager := mocks.NewFakeManagerDefaultFatal(t)
	fakeDownloadManager.BuildProjectHook = func(url string) (project.Project, error) {
		return fakeProject, nil
	}

	err := startCli("p", fakeSender, fakeDownloadManager)
	if err != nil {
		t.Fatal("Good conf should not return err", err)
	}
}

func TestStartCliBuildProjectError(t *testing.T) {
	fakeSender := mocks.NewFakeSenderDefaultFatal(t)

	fakeDownloadManager := mocks.NewFakeManagerDefaultFatal(t)
	fakeDownloadManager.BuildProjectHook = func(url string) (project.Project, error) {
		return nil, errors.New("fake error")
	}

	err := startCli("p", fakeSender, fakeDownloadManager)

	if err == nil {
		t.Fatal("Cli should return error build project")
	}
}
